<?php
/**
 * Electro Child
 *
 * @package electro-child
 */

/**
 * Include all your custom code here
 */

/**
 * Removes header support region.
 */
function remove_header_options() {
    global $electro_options;
    $common_header = $electro_options['header_style'];
    $home_v1 = electro_get_home_v1_meta();
    $page_header = '';
    if( 'v7' === $home_v1['hpc']['header_style'] || 'v7' === $common_header ) {
        remove_action( 'electro_masthead_v3', 'electro_header_support_menu',  10 );
    }
}
add_filter( 'electro_header_style', 'remove_header_options', 10 );
