��          �       l      l     m     }     �     �  0   �     �     �                '     <     W  Y   r  _   �     ,  #   J  
   n  P   y  .   �     �  1  	     ;     R  	   [  E   e  "   �     �     �     �  *     $   G  $   l  *   �  �   �  �   m  B   '	  2   j	     �	  �   �	  =   V
     �
   Default sorting Price Price: Showing search results for: %s Showing the single result Showing all %d results Sort by average rating Sort by latest Sort by popularity Sort by price (asc) Sort by price (desc) Sort by price: high to low Sort by price: low to high Your account was created successfully and a password has been sent to your email address. Your account was created successfully. Your login details have been sent to your email address. Your cart is currently empty. Your current password is incorrect. Your order Your order can no longer be cancelled. Please contact us if you need assistance. You’ve received the following order from %s: min_priceFrom: Project-Id-Version: WooCommerce 3.6.2
Report-Msgid-Bugs-To: https://github.com/woocommerce/woocommerce/issues
POT-Creation-Date: 2019-04-24 17:19:20+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2019-05-02 13:43+0000
Last-Translator: admin <ogegol@gmail.com>
Language-Team: Русский
X-Generator: Loco https://localise.biz/
Language: ru_RU
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10 >= 2 && n%10<=4 &&(n%100<10||n%100 >= 20)? 1 : 2);
X-Loco-Version: 2.2.2; wp-4.9.8 Сортировать Цена Цена: Отображение результатов поиска для: %s Найдено Найдено %d
  По рейтингу Сначала новое По популярности По цене: по возрастанию По цене: по убыванию По цене: по убыванию По цене: по возрастанию Ваша учетная запись была успешно создана, и на ваш адрес электронной почты был отправлен пароль. Ваш аккаунт был успешно создан. Ваши данные для входа были отправлены на ваш адрес электронной почты. Ваша корзина на данный момент пуста. Ваш текущий пароль неверен. Ваш заказ Ваш заказ больше не может быть отменен. Пожалуйста, свяжитесь с нами, если вам нужна помощь. Вы получили следующий заказ от %s:
 От: 